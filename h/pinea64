/*
 * CDDL HEADER START
 *
 * The contents of this file are subject to the terms of the
 * Common Development and Distribution License (the "Licence").
 * You may not use this file except in compliance with the Licence.
 *
 * You can obtain a copy of the licence at
 * RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
 * See the Licence for the specific language governing permissions
 * and limitations under the Licence.
 *
 * When distributing Covered Code, include this CDDL HEADER in each
 * file and include the Licence file. If applicable, add the
 * following below this CDDL HEADER, with the fields enclosed by
 * brackets "[]" replaced with your own identifying information:
 * Portions Copyright [yyyy] [name of copyright owner]
 *
 * CDDL HEADER END
 */
/*
 * Copyright 2019 John Ballance.  All rights reserved.
 * Portions Copyright 2019 Michael Grunditz
 * Portions Copyright 2021 RISC OS Developments Ltd
 * Use is subject to license terms.
 */

#ifndef PINEA64_H
#define PINEA64_H
extern uint32_t EnsureIRQsOff(void);
extern void RestoreIRQs(uint32_t);


// SMHC registers
#define SD_CTRL        (0x00)
#define SD_CLKDIV      (0x04)
#define SD_TMOUT       (0x08)
#define SD_CTYPE       (0x0c)
#define SD_BLKSIZ      (0x10)
#define SD_BYTCNT      (0x14)
#define SD_CMD         (0x18)
#define SD_CMDARG      (0x1c)
#define SD_RESP0       (0x20)
#define SD_RESP1       (0x24)
#define SD_RESP2       (0x28)
#define SD_RESP3       (0x2c)
#define SMHC_INTMASK   (0x30)
#define SMHC_MINTSTS   (0x34)
#define SMHC_RINTSTS   (0x38)
#define SMHC_STATUS    (0x3c)
#define SMHC_FIFOTH    (0x40)
#define SMHC_CTRL      (0x44)
#define SMHC_TBC0      (0x48)
#define SMHC_TBC1      (0x4c)
#define SMHC_CSDC      (0x54)
#define SMHC_A12A      (0x58)
#define SMHC_NTSR_REG  (0x5c)
#define SMHC_HWRST     (0x78)
#define SMHC_BUS_MODE  (0x80)
#define SMHC_DLBA_REG  (0x84)
#define SMHC_IDST      (0x88)
#define SD_IDIE_REG    (0x8c)
#define SMHC_EMMC_DDR_SBIT_DET (0x10c)
#define SMHC_SAMP_DL_REG (0x144)
#define SMHC_DS_DL_REG (0x148)
#define SMHC_FIFO_REG  (0x200)
// further registers at 0x100-0x148

// bits in SD_CTRL reg
#define FIFO_ACC_MOD    (1u<<31)
#define TIME_UNIT_CM    (1u<<12)
#define TIME_UNIT_DA    (1u<<11)
#define DDR_MOD_SEL     (1u<<10)
#define CD_DBC_ENB      (1u<<8)
#define DMA_ENB         (1u<<5)
#define INT_ENB         (1u<<4)
#define DMA_RST         (1u<<2)
#define FIFO_RST        (1u<<1)
#define SOFT_RST        (1u<<0)
#define RESET_BITS      (7<<0)

// bits in SD_CLKDIV register
#define MASK_DATA0      (1u<<31)
#define CCLK_CTRL       (1u<<17)
#define CCLK_ENB        (1u<<16)
#define CDIV_MASK       (0xff)

// bits in SD_CMD, the command register
#define CMD_LOAD        (1u<<31)
#define VOL_SW          (1u<<28)
#define BOOT_ABT        (1u<<27)
#define EXP_BOOT_ACK    (1u<<26)
#define BOOT_MOD_nNORM  (0)
#define BOOT_MOD_MAND   (1u<<24)
#define BOOT_MOD_ALT    (2u<<24)
#define PROG_CLK        (1u<<21)
#define SEND_INIT_SEQ   (1u<<15)
#define STOP_ABT_CMD    (1u<<14)
#define WAIT_PRE_OVER   (1u<<13)
#define STOP_CMD_FLAG   (1u<<12)
#define TRANS_MODE      (1u<<11)
#define TRANS_DIR       (1u<<10)
#define DATA_TRANS      (1u<<9)
#define CHK_RESP_CRC    (1u<<8)
#define LONG_RESP       (1u<<7)
#define RESP_RCV        (1u<<6)

// bits in SMHC_BUS_MODE
#define DES_LOAD_CTRL   (1u<<31)
#define IDMAC_ENB       (1u<<7)
#define FIXED_BUST_CR   (1u<<1)
#define IDMAC_RST       (1u<<0)

// bits in interrupt register
#define INT_CARD_GONE   (1u<<31)
#define INT_CARD_INSERT (1u<<30)
#define INT_SDIO        (1u<<16)
#define INT_DEE         (1u<<15)
#define INT_ACD         (1u<<14)
#define INT_DSE_BC      (1u<<13)
#define INT_CB_IW       (1u<<12)
#define INT_FU_FO       (1u<<11)
#define INT_DSTO_VSD    (1u<<10)
#define INT_DTO_BDS     (1u<<9)
#define INT_RTO_BACK    (1u<<8)
#define INT_DCE         (1u<<7)
#define INT_RCE         (1u<<6)
#define INT_DRR         (1u<<5)
#define INT_DTR         (1u<<4)
#define INT_DTC         (1u<<3)
#define INT_CC          (1u<<2)
#define INT_RE          (1u<<1)

// bits in status register
#define STAT_DMA_REQ       (1<<31)
#define STAT_FSM_BUSY      (1<<10)
#define STAT_CARD_BUSY     (1<<9)
#define STAT_CARD_PRESENT  (1<<8)
#define STAT_FSM_SHIFT     (4)
#define STAT_FSM_MASK      (0xf<<STAT_FSM_SHIFT)
#define STAT_FSM_IDLE      (0<<STAT_FSM_SHIFT)
#define STAT_FSM_INITSEQ   (1<<STAT_FSM_SHIFT)
#define STAT_FSM_CST       (2<<STAT_FSM_SHIFT)
#define STAT_FSM_CTXB      (3<<STAT_FSM_SHIFT)
#define STAT_FSM_CTXIA     (4<<STAT_FSM_SHIFT)
#define STAT_FSM_CCRC      (5<<STAT_FSM_SHIFT)
#define STAT_FSM_CEND      (6<<STAT_FSM_SHIFT)
#define STAT_FSM_RRST      (7<<STAT_FSM_SHIFT)
#define STAT_FSM_RRIR      (8<<STAT_FSM_SHIFT)
#define STAT_FSM_RRT       (9<<STAT_FSM_SHIFT)
#define STAT_FSM_RRCI      (10<<STAT_FSM_SHIFT)
#define STAT_FSM_RRD       (11<<STAT_FSM_SHIFT)
#define STAT_FSM_RRCRC     (12<<STAT_FSM_SHIFT)
#define STAT_FSM_RREND     (13<<STAT_FSM_SHIFT)
#define STAT_FSM_CWNCC     (14<<STAT_FSM_SHIFT)
#define STAT_FSM_WAIT      (15<<STAT_FSM_SHIFT)
#define STAT_FIFO_FULL     (1<<3)
#define STAT_FIFO_EMPTY    (1<<2)
#define STAT_FIFO_TXSPAC   (1<<1)
#define STAT_FIFO_RXDAV    (1<<0)

// SD Card Status register bits
#define SDCS_OUT_OF_RANGE  (1<<31)
#define SDCS_ADDR_ERR      (1<<30)
#define SDCS_BLK_LEN_ERR   (1<<29)
#define SDCS_ERASE_SEQ_ERR (1<<28)
#define SDCS_ERASE_PARAM   (1<<27)
#define SDCS_WP_ERR        (1<<26)
#define SDCS_LOCKED        (1<<25)
#define SDCS_UNLOCK_FAIL   (1<<24)
#define SDCS_COM_CRC_ERR   (1<<23)
#define SDCS_ILLEG_CMD     (1<<22)
#define SDCS_ECC_FAILED    (1<<21)
#define SDCS_CC_ERR        (1<<20)
#define SDCS_ERROR         (1<<19)
#define SDCS_CID_CSD_OVERWR (1<<16)
#define SDCS_WP_ERASE_SKIP (1<<15)
#define SDCS_ECC_DIS       (1<<14)
#define SDCS_ERASE_RESET   (1<<13)
#define SDCS_RDY_FOR_DATA  (1<<8)
#define SDCS_APP_CMD       (1<<5)
#define SDCS_AKE_SEQ_ERR   (1<<3)
#define SDCS_STATE_SHIFT   (9)
#define SDCS_STATE_MASK     (15<<SDCS_STATE_SHIFT)
#define SDCS_STATE_IDLE     (0<<SDCS_STATE_SHIFT)
#define SDCS_STATE_READY    (1<<SDCS_STATE_SHIFT)
#define SDCS_STATE_IDENT    (2<<SDCS_STATE_SHIFT)
#define SDCS_STATE_STDBY    (3<<SDCS_STATE_SHIFT)
#define SDCS_STATE_TRAN     (4<<SDCS_STATE_SHIFT)
#define SDCS_STATE_DATA     (5<<SDCS_STATE_SHIFT)
#define SDCS_STATE_RCV      (6<<SDCS_STATE_SHIFT)
#define SDCS_STATE_PROG     (7<<SDCS_STATE_SHIFT)
#define SDCS_STATE_DIS      (8<<SDCS_STATE_SHIFT)

// bits in CSDS
#define CRC_DET_PARA_OTHER      (0x3u<<0)
#define CRC_DET_PARA_HS400      (0x6u<<0)
#define CRC_DET_PARA_MASK       (0xFu<<0)

// bits in SMHC_NTSR
#define MODE_SELEC                      (1u<<31)
#define SAMPLE_TIMING_PHASE_RX_90       (0u<<4)
#define SAMPLE_TIMING_PHASE_RX_180      (1u<<4)
#define SAMPLE_TIMING_PHASE_RX_270      (2u<<4)
#define SAMPLE_TIMING_PHASE_RX_MASK     (3u<<4)

// bits in EMMC_DDR_SBIT_DET
#define HS_MD_EN             (1u<<31)
#define HALF_START_BIT       (1u<<0)

// bits in SMHC_SAMP_DL_REG
#define SAMP_DL_CAL_START    (1u<<15)
#define SAMP_DL_CAL_DONE     (1u<<14)
#define SAMP_DL_SHIFT        (8)
#define SAMP_DL_MASK         (0x3Fu<<SAMP_DL_SHIFT)
#define SAMP_DL_SW_EN        (1u<<7)
#define SAMP_DL_SW_SHIFT     (0)
#define SAMP_DL_SW_MASK      (0x3Fu<<SAMP_DL_SW_SHIFT)

// bits in DS_DL_REG
#define DS_DL_CAL_START      (1u<<15)
#define DS_DL_CAL_DONE       (1u<<14)
#define DS_DL_SHIFT          (8)
#define DS_DL_MASK           (0x3Fu<<DS_DL_SHIFT)
#define DS_DL_SW_EN          (1u<<7)
#define DS_DL_SW_SHIFT       (0)
#define DS_DL_SW_MASK        (0x3Fu<<DS_DL_SW_SHIFT)


// CCU registers
#define PLL_PERIPH0_CTRL_REG (0x0028)
#define SMHC0_CLK_REG        (0x0088)
#define SMHC1_CLK_REG        (0x008C)
#define SMHC2_CLK_REG        (0x0090)
#define BUS_SOFT_RST_REG0    (0x02C0)

// bits in SMHCn_CLK_REG
#define SCLK_GATING               (1u<<31)
#define CLK_SRC_SEL_OSC24M        (0u<<24)
#define CLK_SRC_SEL_PLL_PERIPH02X (1u<<24)
#define CLK_SRC_SEL_PLL_PERIPH12X (2u<<24)
#define CLK_SRC_SEL_MASK          (3u<<24)
#define CLK_DIV_RATIO_N_SHIFT     (16)
#define CLK_DIV_RATIO_N_MASK      (3u<<CLK_DIV_RATIO_N_SHIFT)
#define CLK_DIV_RATIO_M_SHIFT     (0)
#define CLK_DIV_RATIO_M_MASK      (0xFu<<CLK_DIV_RATIO_M_SHIFT)

// bits in BUS_SOFT_RST_REG0
#define SMHC2_RST_SHIFT      (10)
#define SMHC1_RST_SHIFT      (9)
#define SMHC0_RST_SHIFT      (8)


// Port controller registers
// These are repeated in groups of size 0x24, one for each port
#define Pn_PUL0              (0x1C)

// bits in Pn_PUL0 and Pn_PUL1 (2 bits per pin)
#define Pn_PULL_DISABLE      (0)
#define Pn_PULL_UP           (1)
#define Pn_PULL_DOWN         (2)
#define Pn_PULL_MASK         (3)

// Ports and pins
#define PORT_SMHC0           (5) // port F is index 5 in CPUx-PORT
#define PIN_SMHC0_CMD        (3) // PF3 = SDC0_CMD
#define PIN_SMHC0_CD         (6) // PF6 = SDC0_DET
#define PORT_SMHC1           (6) // port G is index 6 in CPUx-PORT
#define PIN_SMHC1_CMD        (1) // PG1 = SDC1_CMD
#define PORT_SMHC2           (2) // port C is index 2 in CPUx-PORT
#define PIN_SMHC2_CMD        (6) // PC6 = SDC2_CMD
#define PORT_WL_BT           (0) // port L is index 0 in CPUs-PORT
#define PIN_WL_REG_ON        (2) // PL2 = WL_REG_ON (output)
#define PIN_WL_HOST_WAKE     (3) // PL3 = WL_HOST_WAKE (input)
#define PIN_BT_RST_N         (4) // PL4 = BT_RST_N (output)
#define PIN_BT_HOST_WAKE     (5) // PL5 = BT_HOST_WAKE (input)
#define PIN_BT_WAKE          (6) // PL6 = BT_WAKE (output)


#endif

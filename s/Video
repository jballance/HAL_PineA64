; CDDL HEADER START
;
; The contents of this file are subject to the terms of the
; Common Development and Distribution License (the "Licence").
; You may not use this file except in compliance with the Licence.
;
; You can obtain a copy of the licence at
; RiscOS/Sources/HAL/HAL_PineA64/LICENCE.
; See the Licence for the specific language governing permissions
; and limitations under the Licence.
;
; When distributing Covered Code, include this CDDL HEADER in each
; file and include the Licence file. If applicable, add the
; following below this CDDL HEADER, with the fields enclosed by
; brackets "[]" replaced with your own identifying information:
; Portions Copyright [yyyy] [name of copyright owner]
;
; CDDL HEADER END
;
;
; Portions Copyright 2019 Michael Grunditz
; Portions Copyright 2019 John Ballance
; Use is subject to license terms.
;
;
;
;GBLL VideoInHAL
;VideoInHAL SETL {TRUE}
;  	GBLL    Debug
;Debug     SETL    {TRUE}

        GET     Hdr:ListOpts
        GET     Hdr:Macros
        GET     Hdr:System
        GET     Hdr:Machine.<Machine>
        GET     Hdr:ImageSize.<ImageSize>

        GET     Hdr:Proc
        GET     Hdr:OSEntries
        GET     Hdr:HALEntries
        GET     Hdr:GraphicsV
        GET     Hdr:VideoDevice

        GET     PINEA64.hdr
        GET     HDMI.hdr
        GET     StaticWS.hdr

        IMPORT  DebugHALPrint
        IMPORT  DebugHALPrintReg
        IMPORT  DebugHALPrintByte

        AREA    |Asm$$Code|, CODE, READONLY
        IMPORT  memcpy

        EXPORT  VideoDevice_Init
        EXPORT  Video_Init

Video_Init
        MOV     pc, lr

VideoDevice_Init
        ; Not much to do here - just register our HAL device
        Push    "v1,lr"
        ADRL    v1, VideoDevice
        MOV     a1, v1
        ADR     a2, VideoDeviceTemplate
        MOV     a3, #Video_DeviceSize
        BL      memcpy

        ldr     a1, =&10000c  ; RT_Mixer0 Global size register
        ldr     a2, DE_Log
        ldr     a1, [a2,a1]   ; read
        add     a1, a1, #&1<<0
        add     a1, a1, #&1<<16    ; convert to actual width and height
        str     a1, [v1, #LCDde-VideoDeviceTemplate]        ; LCD de

        MOV     a1, v1
        LDR     a1, DE_Log
        str     a1, [v1, #HALde-VideoDeviceTemplate]        ; HAL de
        ; DebugReg a1,"VOPLOG address: "
        ; DebugTX "VOPLOG VIDEODEVICE"

        str      sb, [v1, #HALsb-VideoDeviceTemplate]        ; HAL sb

      [ DMA_CH_count < 8
        ; Use 8th DMA channel for GraphicsV
        LDR     a1, DMAController+HALDevice_Address
        CMP     a1, #0
        ADDNE   a1, a1, #DMA_CH_EN_REG + 7<<DMA_CH_REG_shift
        STR     a1, [v1, #DMAchan-VideoDeviceTemplate]
      ]

        MOV     a1, #0
        MOV     a2, v1
        CallOS  OS_AddDevice
        Pull    "v1,pc"

VideoDeviceTemplate
        DCW     HALDeviceType_Video + HALDeviceVideo_VDU
        DCW     HALDeviceID_VDU_IMX6
        DCD     HALDeviceBus_Sys + HALDeviceSysBus_AXI

        DCD     0               ; API version 0
        DCD     VideoDevice_Desc
        DCD     0               ; Address - filled in later
        %       12              ; Reserved
        DCD     VideoDevice_Activate
        DCD     VideoDevice_Deactivate
        DCD     VideoDevice_Reset
        DCD     VideoDevice_Sleep
        DCD     IRQ_DE          ; Device interrupt
        DCD     0               ; TestIRQ cannot be called
        %       8
        DCD     0               ; Pointer to device-specific field - filled in later
        ASSERT (. - VideoDeviceTemplate) = HALDevice_VDU_Size
        DCD     0
HALsb   DCD     0                ; HAL sb, filled later
        DCD     VideoBacklight   ; routine to set backlight brightness
HALde   DCD     0                ; logical base address of 'video hardware'
LCDde   DCD     0                ; Display resolution.. width<<16 + height
DMAchan DCD     0                ; DMA channel for GraphicsV_Render
        ASSERT (. - VideoDeviceTemplate) = Video_DeviceSize

VideoDevice_Desc
        =       "PineA64 video controller", 0
        ALIGN

; a1-> hal device
; a2 = brightness 0-99  if>99 then just report current value
VideoBacklight
        Entry   "a2-a4,v1-v2,sb"
        ldr     sb, Video_HAL_sb  ; assumes a1->HalDev
        ldr     a3, PWM_Log
        ldr     v1, [a3, #4]
        mov     v2, v1, lsl #16
        mov     v2, v2, lsr #16   ; old value
        mov     v1, v1, lsr #16   ; get 100% value
        mov     a1, #100
        cmp     a2, a1
        bge     %ft01             ; out of range.. just report old/current value
        mul     a2, v1, a2        ; * required
        bl      udivide           ; /100
        orr     a1, a1, v1, lsl #16 ; reassemble
        str     a1, [a3, #4]      ; and back into pwm

01      mov     a2, #100
        mul     a2, v2, a2        ; old value to a2
        mov     a1, v1            ; 100% val
        bl      udivide           ;a1=a2/a1, a2=a2%a1

        EXIT

VideoDevice_Activate
        MOV     a1, #1
        MOV     pc, lr

VideoDevice_Deactivate
VideoDevice_Reset
        MOV     pc, lr

VideoDevice_Sleep
        MOV     a1, #0
        MOV     pc, lr


; unsigned divide routine, lifted from clib
udivide
; Unsigned divide of a2 by a1: returns quotient in a1, remainder in a2
        Entry   "a3,ip"
        MOV     a3, #0
        RSBS    ip, a1, a2, LSR #3
        BCC     u_sh2
        RSBS    ip, a1, a2, LSR #8
        BCC     u_sh7
        MOV     a1, a1, LSL #8
        ORR     a3, a3, #&FF000000
        RSBS    ip, a1, a2, LSR #4
        BCC     u_sh3
        RSBS    ip, a1, a2, LSR #8
        BCC     u_sh7
        MOV     a1, a1, LSL #8
        ORR     a3, a3, #&00FF0000
        RSBS    ip, a1, a2, LSR #8
        MOVCS   a1, a1, LSL #8
        ORRCS   a3, a3, #&0000FF00
        RSBS    ip, a1, a2, LSR #4
        BCC     u_sh3
        RSBS    ip, a1, #0
        BCS     dividebyzero
u_loop  MOVCS   a1, a1, LSR #8
u_sh7   RSBS    ip, a1, a2, LSR #7
        SUBCS   a2, a2, a1, LSL #7
        ADC     a3, a3, a3
u_sh6   RSBS    ip, a1, a2, LSR #6
        SUBCS   a2, a2, a1, LSL #6
        ADC     a3, a3, a3
u_sh5   RSBS    ip, a1, a2, LSR #5
        SUBCS   a2, a2, a1, LSL #5
        ADC     a3, a3, a3
u_sh4   RSBS    ip, a1, a2, LSR #4
        SUBCS   a2, a2, a1, LSL #4
        ADC     a3, a3, a3
u_sh3   RSBS    ip, a1, a2, LSR #3
        SUBCS   a2, a2, a1, LSL #3
        ADC     a3, a3, a3
u_sh2   RSBS    ip, a1, a2, LSR #2
        SUBCS   a2, a2, a1, LSL #2
        ADC     a3, a3, a3
u_sh1   RSBS    ip, a1, a2, LSR #1
        SUBCS   a2, a2, a1, LSL #1
        ADC     a3, a3, a3
u_sh0   RSBS    ip, a1, a2
        SUBCS   a2, a2, a1
        ADCS    a3, a3, a3
        BCS     u_loop
        MOV     a1, a3
        EXIT
dividebyzero                            ; for our use .. not really trapping divide by zero
        mov     a1, #0
        mov     a2, #0
        EXIT


        END
